### git 

- 版本管理工具
- 命令行工具

### 安装
1. windows 需要单独安装
2. mac 电脑自带的

### 检测安装成功
```shell
git --version
# git version 2.32.0 (Apple Git-132)
```

### 怎么管理文件

1. git init 
在当前目录下会产生.git文件夹， 不允许子目录中有.git, 会嵌套管理，管理所有目录

### 怎么提交

- 本地工作区（本地文件夹）
- 暂存区
- 历史版本区 （产生文件提交的版本号）
以上是本地管理
- 远程仓库 （第三方服务）

本地工作区 -》（git add 路径/-A） 暂存区  -》（git commit -m "message提交信息"） 历史版本区

历史版本区 -》 （git push）远程仓库

### 查看历史提交记录

git log

### 回滚文件状态

init -> docs: upd(head) -> upd: index.js -> docs: upd1(head)

1. git reset --hard HEAD~6/commitId  直接回到回滚的commitId，之后所有记录不显示
2. git revert --no-commit HEAD~6/commitId 找到之前的文件状态，拉取之前的文件状态，手动修改文件，得到你想要的文件状态，再次提交

通过commit
通过head

### 推送

1. 本地仓库跟远程仓库关联

- 本地仓库已经存在， 通过git remote add origin 仓库地址，关联远程仓库

- 本地仓库不存在。直接克隆远程仓库，自动关联

http协议

ssh协议 -》 需要配置秘钥

### 查看文件状态

git status 
### 可视化工具

sourcetree 


### 多人协作开发

- 解决冲突
碰到冲突，手动解决冲突之后再次提交

- 使用分支进行开发
创建开发分支，多人开发，创建自身开发分支 development
测试分支 test
预发分支 release
合到主分支 master


1. 创建本地分支
```shell
git branch branchname
```

切换分支
```shell
git checkout branchname
```

创建并切换分支
```shell
git checkout -b branchname
```

合并分支
```shell
git merge 要合并的分支代码
```

2. 查看本地分支
```shell
git branch
```

3. 查看所有分支
```shell
git branch --all
```

4. 查看远程分支
```shell
git branch -r
```

5. 分支冲突
1
1
1
1
