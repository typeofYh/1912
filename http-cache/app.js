const http = require('http');
const path = require('path');
const publicPath = (filename) => path.join(__dirname,'public', filename);
const fs = require('fs');
let etag = 'default';
fs.watchFile(publicPath('index.js'), (curr, prev) => {
  // console.log(curr, prev)
  // ;
  etag = curr.atimeMs.toString();
})
http.createServer((req,res) => {
  console.log(req.url);
  if(req.url === '/favicon.ico'){
    res.end('');
    return;
  }
  if(req.url === '/'){
    res.end(fs.readFileSync(publicPath('index.html')));
    return;
  }
  if(req.url === '/index.js'){
    // 第一次服务器返回给浏览器
    res.setHeader('cache-control','max-age=20')
    res.setHeader('etag', etag);
    res.setHeader('last-modified', new Date().toGMTString());
    // 强缓存失效 再次请求/index.js 先取If-None-Match
    // console.log(req.headers);
    const curEtag = req.headers['if-none-match'];
    const curTime = req.headers['if-modified-since'];
    if(curEtag === etag/**文件没有变化 */){
      res.statusCode = 304;
      res.end();
    }else {
      res.statusCode = 200;
      res.end(fs.readFileSync(publicPath('index.js')));
    }
    return;
  }
}).listen(4000, () => {
  console.log('port is 4000')
})