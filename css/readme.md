1. css选择器
通配符 标签选择 类名选择器 层级选择器 id选择器 行内样式style属性 !important
2. 选择器的优先级

3. position有几种value，分别的作用是什么
absolute relative fixed sticky（粘性）

4. 实现两列布局，左侧固定宽度，右侧自适应(./layout.html)

4. bfc规范
Formatting context(格式化上下文) ， 形成独立容器的规则里面元素不会影响外部元素

5. vw vh 100% px rem em 分别代表的意思
vw viewWidth 视口宽度单位
vh 视口高度单位
百分比继承父元素进行计算
px 是绝对单位，像素单位，代表屏幕最小像素
rem 是相对单位是相对html的fontSize进行计算
em 是相对单位是相对父元素的fantSize进行计算

6. 如何实现一个未知宽高的元素水平垂直居中？(./center.html)
