const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
const body = require('koa-body');
const static = require('koa-static');
const fs = require('fs');
const xlsx = require('node-xlsx');
const path = require('path')
// app.use();
app.use(static('./static'));
app.use(router.routes(),router.allowedMethods())

const saveFile = (readFile, writeFile) => {
  return new Promise((resolve) => {
    readFile.pipe(writeFile);
    readFile.on('end', () => {
      resolve();
    })
  })
}
const clomus = [
  {
    name: "订单编号",
    key: "order"
  },
  {
    name: "下单时间",
    key: "createTime"
  },
  {
    name: "收件人",
    key: "pullUserName"
  },
  {
    name: "手机",
    key: "phone"
  },
  {
    name: "收货地址",
    key: "address"
  },
  {
    name: "商品名称",
    key: "shopName"
  },
  {
    name: "数量",
    key: "total"
  }
];
const formatTable = ([{data}]) => {
  return data.slice(2).map((item) => {
    console.log(item);
    return item.reduce((pre, val, i) => {
      pre[clomus[i]['key']] = val;
      return pre
    },{})
  })
}
router.post('/upload', body({
  multipart:true
}), async (ctx) => {
  const { fileName } = ctx.request.body;
  const { file } = ctx.request.files;
  const newFileName = `${fileName}_${Date.now()}_${Math.round(Math.random() * 10000)}${file.originalFilename.match(/\.\w+$/)[0]}`
  // console.log(ctx.request.body, ctx.request.files, newFileName);
  // const readFile = fs.createReadStream(file.filepath);
  // const writeFile = fs.createWriteStream('./static/' + newFileName);
  // await saveFile(readFile,writeFile);
  const workSheetsFromBuffer = xlsx.parse(fs.readFileSync(file.filepath));
  // console.log();/
  const data = formatTable(workSheetsFromBuffer);
  fs.writeFileSync('./data.json', JSON.stringify(data, null, 2), 'utf-8')
  
  ctx.body = {
    code: 200,
    status: 'success',
    filePath: '/static/'+newFileName,
    data
  }
})

router.post('/download', body({
  multipart:true
}), async (ctx) => {
  const {body} = ctx.request;
  const data = JSON.parse(fs.readFileSync('./data.json', 'utf8')).filter(item => body.includes(item.order)).map(item => Object.values(item));
  // 把这个数据生成一个表格文件
  const res = xlsx.build([{name: 'mySheetName', data: data}]);
  console.log(res);
  ctx.body = res;
})
// 上传单个切片
router.post('/uploadChunk', body({
  multipart:true
}), ctx => {
  const { chunkName, fileName } = ctx.request.body;
  const dirPath = './static/bigfile/'+fileName+'_chunkDir/';
  !fs.existsSync(dirPath) && fs.mkdirSync(dirPath);
  fs.writeFileSync(dirPath + chunkName, fs.readFileSync(ctx.request.files.fileData.filepath))
  ctx.body = {
    success: true
  }
})
// 合切片
router.post('/uploadEnd', body({
  multipart:true
}), async (ctx) => {
  const { fileName } = ctx.request.body;
  // 找切片合切片
  // 读取切片 读目录
  const dirPath = `./static/bigfile/${fileName}_chunkDir`;
  const dirs = fs.readdirSync(dirPath);
  const dirPromise = dirs.map(filePath => {
    return new Promise((resolve) => {
      const [index, start] = filePath.split('_');
      filePath = path.join(dirPath, filePath);
      const read = fs.createReadStream(filePath); // 所有切片内容
      const write = fs.createWriteStream('./static/bigfile/' + fileName, {
        start: start * 1
      });
      read.pipe(write);
      write.on('finish', () => {
        fs.unlinkSync(filePath);
        resolve();
      })
    })
  })
  await Promise.allSettled(dirPromise)
  fs.rmdirSync(dirPath);
  // 返回文件地址给前端
  ctx.body = {
    success: true,
    filePath: ''
  }
})

app.listen(3000, function(){
  console.log('server port is 3000');
})