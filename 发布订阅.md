```js

class Vue {
  events = {}
  $on(callbackName, callbackFun){    // {test:[fn,fn]}
     console.log(callbackName, callbackFun, this.events);
    if(!Array.isArray(this.events[callbackName])){
      this.events[callbackName] = [callbackFun];
    } else {
      this.events[callbackName].push(callbackFun); 
    }
  }
  $emit(callbackName, ...arg){
    this.events[callbackName].forEach(item => {
      item(...arg);
    })
  }
}

const $bus = new Vue();


$bus.$on('test', function(){
  console.log(1)
})
$bus.$on('test', function(){
  console.log(2)
})
$bus.$emit('test', '参数')

```