/**
 * 统计"aaa,bbb,cad,bdd"
 * {a:4, b:4, c:1,d:2,,3}
 */

function getStringCount(str){
  return str.split('').reduce((val, item) => {
    val[item] = val[item] ? val[item] + 1 : 1;
    return val;
  }, {})
}

console.log(getStringCount("aaa,bbb,cad,bdd"));

/**
 * 'http://wwww.baidu.com/api?id=10&name=zs'
 * {id:10,name:'zs'}
 */

function formatUrlParams(url){
  const paramsString = url.split('?')[1];
  return paramsString.split('&').reduce((val,item) => {
    const [key, value] = item.split('=');
    val[key] = value;
    return val;
  }, {})
}
console.log(formatUrlParams('http://wwww.baidu.com/api?id=10&name=zs'));


function ObjectParams(url, queryObj){
  return `${url}?${Object.keys(queryObj).map(key => `${key}=${queryObj[key]}`).join('&')}`
}
console.log(ObjectParams('http://wwww.baidu.com/api',{ id: '10', name: 'zs' }));


