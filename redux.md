## redux是一个状态管理工具


1. reducer是一个纯函数（不受外部状态影响的函数） 接受action，计算state返回仓库最新状态
2. 只有唯一的根state
3. state是只读，只能通过dispatch派发action触发reducer计算返回


## redux怎么跟react结合使用
通过react-redux这个包

提供了Provider组件和connect高阶组件


Provider组件，只能当做根组件使用包裹了页面所有的组件，调用了React.createContext().Provider向子元素组件下发store

connect高阶组件，通过React.createContext().consumer取到store，向原组件的props注入仓库状态和dispatch，