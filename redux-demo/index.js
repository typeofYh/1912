const { createStore } = require('redux');

const reducer = (state = {count:0}, action) => {
  console.log('reducer', action);
  switch(action.type){
    case 'ADD':
      return {
        ...state,
        count: state.count + 1
      }
    case 'DIS':
      return {
        ...state,
        count: state.count - 1
      }
    default: 
      return state
  }
}

const store = createStore(reducer);  // store仓库实例 默认执行一次reducer
// export default store;




console.log(store.getState());

store.dispatch({
  type: 'ADD'
})


console.log(store.getState());

const context = React.createContext();
const Provider = ({store, children}) => {
  return <context.Provider value={store}>
    {children}
  </context.Provider>
}

const connect = (mapStateToProps,mapDispatchToProps) => {
  // const store = React.useContext(context)
  return (Component) => {
    {
      <context.Consumer>
        {
          (store) => {
            return <Component {...store.getState()} dispatch={store.dispatch} />
          }
        }
      </context.Consumer>
    }
  }
}

connect()(Com)

store.dispatch({
  type: 'ADD'
})

console.log(store.getState());
