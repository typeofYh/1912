const resource = { a: 1, b: { c: 2, d: { e: 3, f: 4 }}, g: [5, 6, { h: 7 }]};

const isType = (val) => Object.prototype.toString.call(val).split(' ')[1].slice(0, -1)
const getKeyName = (keyName, data, res) => {
  if(isType(data) === 'Array'){
    data.map((item, i) => {
      if(typeof item === 'object'){
        keyName += `${i}.`
        getKeyName(keyName, item, res);
      } else {
        keyName += `${i}`;
        res[keyName] = item;
      }
    })
    return;
  }
  if(isType(data) === 'Object'){
    Object.keys(data).map(key => {
      if(typeof data[key] === 'object'){
        keyName += `${key}.`
        getKeyName(keyName, data[key], res);
      } else {
        keyName += key;
        res[keyName] = data[key];
      }
    })
    return;
  }
  
}
function formatData (data) {
  let res = {};
  if(typeof data === 'object'){
    getKeyName('', data, res);
  }
  return res;
}
console.log(formatData(resource));

// {
//   "a": 1, "b.c": 2,
//   "b.d.e": 3, "b.d.f": 4, 
//   "g.0": 5, "g.1": 6, "g.2.h": 7
// }



