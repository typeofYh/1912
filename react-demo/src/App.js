import { useState } from 'react';
import Table from "./components/table"
import {upload, download, uploadChunk, uploadEnd} from "./services/api"
import app2 from "./app.module.css"
import { clomus, chunkSize } from "./config"

function App() {
  const [tableData, setTableData] = useState([]);
  const hanldechange = async (e) => { 
    // console.log(e.target.files[0]);
    const formData = new FormData();
    formData.append('file', e.target.files[0]);
    formData.append('fileName', '表格');
    const {data} = await upload(formData);  
    // console.log(data);
    setTableData(data.data);
  }
  const hanldeClickDownload = async () => {
    const data = await download([1,3])
    console.log(data.data);
    const urlblob = URL.createObjectURL(data.data); 
    const createA = document.createElement('a');
    createA.href = urlblob;
    createA.download = '表格.xls';
    document.body.append(createA);
    createA.click();
    window.URL.revokeObjectURL(urlblob);
    // document.body.removeChild(createA);
  }
  const handleBigFile = async (e) => {
    const file = e.target.files[0];
    // 计算切片数量
    const chunkCount = Math.ceil(file.size / chunkSize);
    // 切分file对象
    const allChunk = [];
    for(let i = 0; i < chunkCount; i++){
      const formData = new FormData();
      const start = i * chunkSize;
      formData.append('index',i);
      formData.append('fileName', file.name);
      formData.append('chunkSize', chunkSize);
      formData.append('chunkName', `${i}_${start}_${file.name}`);
      formData.append('fileData', file.slice(start, start + chunkSize));
      allChunk.push(formData);
    }
    // 30
    await Promise.allSettled(allChunk.map(data => uploadChunk(data)))
    // 通知后端切片传完了，和切片
    await uploadEnd({
      fileName: file.name
    });
  }
  return (
    <div className={app2.app}>
      <input type="file" onChange={hanldechange}/>
      <button onClick={hanldeClickDownload}>批量下载</button>
      {/* <a href='http://localhost:3000/表格_1665718091955_1768.xls' download={'表格1.xls'}>批量url下载</a> */}
      <Table 
        rowKey='order'
        dataSource={tableData}
        cloumns={clomus}
      />
      <input type="file" onChange={handleBigFile}/><span>大文件上传</span>
    </div>
  )
}

export default App;
