import axios from "axios"
export const upload = (data) => {
  return axios.post('/upload', data)
}

export const download = (ids) => {
  return axios.post('/download', ids , {
    responseType: 'blob'
  })
}

// 上传切片
export const uploadChunk = (data) => {
  return axios.post('/uploadChunk', data);
}

// 合并切片
export const uploadEnd = (data) => {
  return axios.post('/uploadEnd', data);
}