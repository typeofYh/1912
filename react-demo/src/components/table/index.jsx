const Table = ({dataSource = [], cloumns = [], rowKey = ''}) => {
  return (
    <table>
      {
        cloumns.length ? (
          <th>
            {
              cloumns.map(item => (
                <td key={item.dataIndex}>
                  {item.title}
                </td>
              ))
            }
          </th>
        ) : null
      }
      <tbody>
        {
          dataSource.length ? (
            dataSource.map(item => (  // item {}
              <tr key={rowKey ? item[rowKey] : item.key}>
                {
                  cloumns.map(val => (
                    <td key={val.dataIndex}>
                      {item[val.dataIndex]}
                    </td>
                  ))
                }
              </tr>
            ))
          ) : <tr>暂无数据</tr>
        }
      </tbody>
    </table>
  )
}

export default Table;