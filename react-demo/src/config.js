export const clomus = [
  {
    title: "订单编号",
    dataIndex: "order"
  },
  {
    title: "下单时间",
    dataIndex: "createTime"
  },
  {
    title: "收件人",
    dataIndex: "pullUserName"
  },
  {
    title: "手机",
    dataIndex: "phone"
  },
  {
    title: "收货地址",
    dataIndex: "address"
  },
  {
    title: "商品名称",
    dataIndex: "shopName"
  },
  {
    title: "数量",
    dataIndex: "total"
  }
];

// 切片大小 字节 
export const chunkSize =  1024 * 1024 * 10;  