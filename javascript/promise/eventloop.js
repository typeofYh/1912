console.log(1)  // 同步
setTimeout(() => {
  console.log(4)
  Promise.resolve('8').then(res => {
    console.log(res)
    setTimeout(() => {
      console.log('10')
    },0)
  })
}, 0)
new Promise((resolve, reject) => {
  console.log(2) // 同步
  setTimeout(() => {
    resolve(3)
  }, 10)
  reject(11)
}).then(res => {   // setTimout 0 setTimout 10 Promise.resolve('8') .then
  console.log(res)
}).catch(error => {
  console.log(error)
})
console.log(6)  // 同步

// .then setTimeout

// 1 2 6 3 4 