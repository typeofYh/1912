## es6新增
1. let const var
2. 箭头函数
 - 箭头函数的this不会发生改变，this指向取定义箭头函数的上下文对象
 - 箭头函数不能当做构造函数来使用
    1. 箭头函数的this指向不能修改
    2. 箭头函数没prototype属性
 - 箭头函数内不能使用arguments，可以使用...扩展运算符来代替
 - 箭头函数写法可以省略return

 ```js
  const reduceSum = arr => arr.reduce((def,val) => def+val, arr[0]);
  const arr = [1,2,3];
  const format = arr.map(item => ({name:item})); //[{name:1},{name:2},{name:3}]
 ```

 new关键字会干几件事？
 1. 创建实例对象
 2. 调用函数
 3. 修改函数的this指向，指向实例对象
 4. 返回实例对象

3. this指向
  当前运行环境的上下文对象，在函数调用时形成，根据不同的调用方式形成不同的this指向

```js
  var title = 'window';
  const a = {
    title: 'a',
    run(key, val){
      console.log(this.title)
      key && val && (this[key] = val);
      console.log(this);
    },
  }
  a.run(); // a this指向a对象， 方法调用：this指向拥有该方法的对象 obj.xxx()
  const run = function(){
    console.log(this.title) // window.title  
  }
  run(); // 普通函数调用： this指向window（在严格模式下指向undefined)
  const run1 = a.run;
  run1() // 普通函数调用： this指向window（在严格模式下指向undefined)
  // 隐式调用 apply call
  const b = {};
  a.run.call(b, 'name', 'b'); // call方法帮助调用a.run,把this指向b，给b传了name b
  run1.apply(b, ['age', '13']); // apply方法帮助调用run1,把this指向b，给b传了age 13
  a.run.apply(null, ['aaa', 'aaa']); 
```

4. call\apply\bind

```js
Function.prototype.myCall = function(targetThis, ...arg){
  // console.log(this);
  const souceFunction = this; // 函数a
  const functionName = `_${souceFunction.name}_${Date.now()}`;
  // 调用souceFunction 修改this指向 targetThis
  targetThis[functionName] = souceFunction;
  targetThis[functionName](...arg); // 方法调用
  delete targetThis[functionName];
}

const a = function(key,value){
  console.log(this, key, value);
}
const o = {};
a('name1','window');
// a是函数对象
a.myCall(o, 'name', 'o')  // a.__proto__.myCall  Function.prototype.myCall
```

5. class extends super prototype static

```js
class A {
  
}

const o_a = new A();

// o_a 是实例对象 上有 __proto__
// A 是构造函数（通过new关键字调用的函数是构造函数） typeOf A === 'function'  true  函数对象有prototype
// o_a.__proto__ === A.prototype 
```

- 原型链
实例对象的__proto__一直往下找的一条链，叫原型链
原型的优点：把公共方法和属性放到原型，在重复创建实例时，可以节省内存开销。

- class 是es6提供的面向对象的语法糖，可以简化面向对象语法
- es6实现继承通过extends来实现继承，super调用父类，修改父类this，调用super之后才能使用this
- es5实现继承通过借用构造函数（在当前构造函数中调用父类，修改父类this指向） 原型继承 当前原型等于父类原型 
组合继承 借用构造函数+原型继承  修改子类原型影响父类
寄生组合式继承 借用构造函数+拷贝原型继承 最终通过它来实现继承

6. **promise**
promise解决了异步编程回调嵌套，回调地狱的问题，promise怎么实现链式调用。不断返回新的promise实例实现链式调用

promise是一个包裹异步操作的一个容器，它有3种状态：padding，reject，resolve三种状态，可以从padding到成功，或者padding到失败，状态一旦发生变化，会执行对应 then方法和catch方法

7. async 函数 
异步脚本控制器，可以把await看成一个节点，每次等到await右边和上面的内容执行完成，才会向下执行
async返回值是一个promise对象

8. Generator 函数
以*号来声明函数的，调用方式跟普通函数一样，返回值是个(迭代器对象)

async函数是Generator函数的语法糖，async函数可以自动向下执行await，但是Generator 需要手动调用next往下执行

9. for of iterator

引入了for...of循环，作为遍历所有数据结构的统一的方法


10. 数据结构 set map weakSet weakMap

set 数据结构没有重复项
map 任意类型的值都可以当做key来使用

weakMap和Map的区别
1. Map任意类型值都可以当做key, weakMap只能对象当做key
2. weakMap作为key的对象是对对象的弱引用，不会增加引用计数

队列：先进先出
栈：先进后出
链表： 通过指针查找元素
树：二叉树和绝对二叉树，二叉树的遍历

js的垃圾回收机制：
- 标记清除法
```js
function a(){
  let str = 'string';
}
a()
console.log(str);
```
- 引用计数法
```js
let o = {};
let a = o;
a.name = 'a';

```

11. modules cmd、 commonjs 、 esModule 

```
js
  index.js
common
  jquery.js
  day.js
  xxx.js
  a.js
  b.js
  c.js
index.html  <script src="./js/jquery.js"></script><script src="./js/index.js"></script>
list.html
detail.html
```
require.js 通过创建script实现模块管理，只能在浏览器端使用
  <script src="require.js" main="index.js"></script>

commonjs 规范，只能在node端使用
const a = require('./a.js')

module.export = {}

esModule 解决前端模块管理方式统一的问题
import 

export


12. webpack（umd） vite 

## 打包原理
1. 根据配置文件读取入口文件路径
2. 读取入口文件内容，根据ast（抽象语法树），递归生成__webpack_modules__所有的文件依赖对象，文件路径为key，value是一个函数
3. 自定义require方法，通过eval语句实现不断递归调用require方法

## loader的作用
loader就是一个函数，modules:rules:test://,只要文件符合正则规格，会调用该函数（loader），该函数接受源文件内容，返回新文件内容

## plugin的作用
监听webpack打包的hooks（钩子函数 特定时间执行的函数），在特定时间改变输出结果

## plugin跟loader最大的区别，loader是检测到文件时直接调用按顺序执行，plugin可以监听webpack打包特定时间内执行


dns
cdn
tcp
http
https
dom render
style render


浏览器缓存
  强缓存
  协商缓存 304
    etag
    Last-Modified


## 在url输入地址浏览器会干哪些事

解析 www.baidu.com -> 103.235.46.40:443   dns 解析
dns域名解析器：
 1. 本地域名解析器 hosts文件
 2. 找缓存
 3. .com 根域名解析器
得到ip地址，建立tcp链接，3次握手
浏览器发送syn -> syn + ack -> ack
发起http请求 header和空行和body 
  - 强缓存，命中直接读取浏览器缓存，不会向服务器发请求
    responseHeader：
    cache-control：
      max-age=xxx（s） 相对第一次请求时间进行计算， 缓存的内容将在 xxx 秒后失效, http1.1
      public 所有资源都会缓存
      private 代理服务器不能缓存
      no-cache 不缓存会进行协商缓存验证
      no-store 不会缓存
    expires:  http1.0的headers
      值是GMT的绝对时间
  - 协商缓存
    强缓存失效，向服务器发送请求, 会根据etag和last-modified的值判断文件是否更新，更新返回新资源和200，没有更新返回304就不会返回资源了
    responseHeader：
     etag: 文件内容的hash值
     last-modified: 文件最后一次修改时间
    requestHeader：
      if-none-match： 上一次返回的etag
      if-modified-since： 上一次返回的last-modified

4. http响应之后自动断开链接
5. tcp进行4次挥手
  浏览器告诉服务器要断开 发送 fin 
  服务器告诉浏览器接到断开信号，查询数据传输返回 fin + ack
  服务器告诉浏览器传输完毕 可以关闭链接了 fin
  浏览器告诉服务器关闭了 ack
6. 浏览器渲染页面


## 前端性能优化

- web层面 服务器层面
  http缓存
  http传输压缩 gzip压缩
  cdn 内容分发网络

- 代码层面
  减少http请求
    分页加载
    图片懒加载
    css 雪碧图
    图片分场景加载 小图加载base64 大图发送http请求
  封装公共组件和方法减少重复代码
  预加载
  大文件分片上传
  
- 打包压缩层面