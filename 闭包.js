
function run(){
  let count = 0;
  return () => {
    // 在这个函数作用域内定义了count
    count++; // count = count + 1;
    console.log(count);
  }
}
const _run = run();

_run(); // 1
_run(); // 2
_run(); // 3
_run(); // 4

run()() // 1
run()() // 1
run()() // 1
run()() // 1

// 闭包： 函数内返回的函数，用来访问函数内部变量的函数叫闭包
// 形成闭包：函数内部的函数，在外部引用就会形成闭包

// 闭包的使用场景

// 自执行函数，保证不污染全局变量形成模块
// const a = (function({name}){
//   const context = require(name);
//   return function(){
//     context.xx = 'aa'
//   }
// })({name:'a'})

// 防抖 和 节流
/*
<button class="btn">click</button>

const btn = document.querySelector('.btn');

function debounce (ck){
  let timer = null;
  return function(e){ // 点击的时候 真正的事件处理程序
    timer = setTimeout(() => {
      ck(e);
    }, 100)
  }
}
btn.onclick = debounce(function(){

})
*/

/*
useEffect(() => {
  timer = setTimeout(() => {

  }, 1000)
  return () => { // 
    clearTimeout(timer);
  }
})
*/

for(var i=0;i<5;i++){
  (function(n){
    setTimeout(() => {
      console.log(n) // 0 - 4
    })
  })(i)
}